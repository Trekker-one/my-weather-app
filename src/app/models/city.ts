export class City{
    constructor(
        public country: string,
        public city: string,
        public imageUrl: string
    ){}

}