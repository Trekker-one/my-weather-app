import { Component, OnInit } from '@angular/core';
import { WeatherService } from '../services/weather.service';
import { City } from '../models/city';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-city',
  templateUrl: './add-city.component.html',
  styleUrls: ['./add-city.component.css']
})
export class AddCityComponent implements OnInit {
  model: City;

  cities_to_add = [
    {country: 'China', city: 'WuHan', imageUrl: `https://upload.wikimedia.org/wikipedia/commons/thumb/e/e6/China_Jiangsu_adm_location_map.svg/250px-China_Jiangsu_adm_location_map.svg.png`} as City,
    {country: 'China', city: 'ChangSha', imageUrl: `https://upload.wikimedia.org/wikipedia/commons/thumb/e/e6/China_Jiangsu_adm_location_map.svg/250px-China_Jiangsu_adm_location_map.svg.png`} as City,
    {country: 'China', city: 'GuiYang', imageUrl: `https://upload.wikimedia.org/wikipedia/commons/thumb/e/e6/China_Jiangsu_adm_location_map.svg/250px-China_Jiangsu_adm_location_map.svg.png`} as City,
    {country: 'China', city: 'ChengDu', imageUrl: `https://upload.wikimedia.org/wikipedia/commons/thumb/e/e6/China_Jiangsu_adm_location_map.svg/250px-China_Jiangsu_adm_location_map.svg.png`} as City
  ]

  constructor(private weatherSvc: WeatherService, private router: Router) {
    this.model = new City("","","");
   }

  ngOnInit(): void {

  }

  onSubmit(){
    for(let i = 0; i < this.cities_to_add.length; i++){
      if(this.model.city === this.cities_to_add[i].city){
        this.model.country = this.cities_to_add[i].country;
        this.model.imageUrl = this.cities_to_add[i].imageUrl;
      }
    }
    this.weatherSvc.addCity(this.model);
    this.router.navigate(['/']);
  }

}
